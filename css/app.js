function validate(){
    let name = document.forms["form"]["userName"].value;
    let email = document.forms["form"]["email"].value;

    if (name.length===0){
        document.getElementById("userNameFn").innerHTML="*fill the form";
        return false;
    }

    if (email.length===0){
        document.getElementById("emailFn").innerHTML="*fill the form";
        return false;
    }

    let atPosition = email.indexOf("@");
    let dotPosition = email.indexOf(".");

    if (atPosition <1|| dotPosition <1){
        document.getElementById("emailFn").innerHTML="*email entered is not correct";
        return false;
    }
}

// menu
function myFunction() {
    let x = document.getElementById("myTopnav");
    if (x.className === "topnav"){
        x.className += " responsive";
    }else {
        x.className = "topnav";
    }
}

let filter = document.querySelectorAll('.filter li');

filter.forEach((fil) => {
    fil.addEventListener('click', changeFilters)
});

function changeFilters(e) {
    let currentClass = e.target.classList.value;

    let portfolio = document.getElementsByClassName('all');
    for (let i=0; i<portfolio.length; i++) {
        if (portfolio[i].classList.contains(currentClass)) {
            portfolio[i].style.display="block";
        } else {
            portfolio[i].style.display="none";
        }
    }
}
// slider
let buttonLeft = document.querySelectorAll('.btn-left');
let buttonRight = document.querySelectorAll('.btn-right');

function goToNext() {
    let list = document.querySelector('.list-inform');
    let margin = parseInt(list.style.marginLeft);
    if(margin > -198) {
        let newMargin = margin - 99;
        list.style.marginLeft = newMargin + 'vw';
    }
}

for(let i = 0; i < buttonLeft .length; i++) {
    buttonLeft [i].addEventListener("click", goToNext);
}

function goToBack() {
    let list = document.querySelector('.list-inform');
    let margin = parseInt(list.style.marginLeft);
    if(margin < 0) {
        let newMargin = margin + 99;
        list.style.marginLeft = newMargin + 'vw';
    }
}

for(let i = 0; i < buttonRight.length; i++) {
    buttonRight[i].addEventListener("click", goToBack);
}

// Tab
let tabText = document.querySelectorAll('.tabText');
let tab = document.querySelectorAll('.tab');
let tabs = document.getElementById('tabs');
hideTabsText(1);

function hideTabsText(a) {
    for(var i = a; i < tabText.length; i++){
        tabText[i].classList.remove('show');
        tabText[i].classList.add('hide');
        tab[i].classList.remove('active-icon');
    }
}
tabs.addEventListener('click', (e) => {
    let target = e.target;
    if(target.className === 'tab'){
        for(var i = 0; i < tab.length; i++){
            if(target === tab[i]){
                showTabsText(i);
                break;
            }
        }
    }
    if(target.classList.contains('tab-cont')){
        for(var i = 0; i < tab.length; i++){
            if (target.parentElement === tab[i]){
                showTabsText(i);
                break;
            }
        }
    }
});
function showTabsText(b) {
    if(tabText[b].classList.contains('hide')){
        hideTabsText(0);
        tab[b].classList.add('active-icon');
        tabText[b].classList.remove('hide');
        tabText[b].classList.add('show');

    }

}